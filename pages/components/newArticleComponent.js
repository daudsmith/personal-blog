/* eslint-disable */

import React, { useEffect, useState } from "react";
import Image from 'next/image'
import styles from '../../styles/Home.module.css'
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import { CardActionArea } from '@mui/material';
import { motion } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

function NewArticleComponent(props) {
    const API = "http://localhost:1337"
    const [result, setResult] = useState(props.data);

    const [ref, inView] = useInView({
        triggerOnce: false,
        threshold: 0.5,
    });
    
    return (
        <Box className={styles.newArticleBoxStyle}>
            <Image
                src="/images/line-left-white.png"
                width={800}
                height={800}
                loading="lazy"
                alt="Image"
                className={styles.newArticleLineStyle}/>
            <Paper elevation={0} className={styles.newArticlePaperStyle}>
            <motion.div
                    ref={ref}
                    className={`popup ${inView ? 'visible' : ''}`}
                    initial={{ opacity: 0, scale: 0.8 }}
                    animate={{ opacity: inView ? 1 : 0, scale: inView ? 1 : 0.8 }}
                    transition={{ duration: 1, ease: 'easeInOut' }}
                >
                <Typography className={styles.newArticleTextPrimaryStyle}>
                    New Article
                </Typography>
            </motion.div>
            </Paper>
            <Grid container>
                <motion.div
                    ref={ref}
                    className={`popup ${inView ? 'visible' : ''}`}
                    initial={{ opacity: 0, scale: 0.8 }}
                    animate={{ opacity: inView ? 1 : 0, scale: inView ? 1 : 0.2 }}
                    transition={{ duration: 1, ease: 'easeInOut' }}
                >
                    <Box className={styles.newArticleCardBoxStyle}>
                    <Grid container spacing={2}> 
                        {result.map((data) => (
                        <Grid item xs={12} sm={6} md={3} key={data.id}>
                            <motion.div
                            className="zoom"
                            whileHover={{ scale: 1.1 }}
                            >
                            <Card className={styles.newArticleCardStyle}>
                                <CardActionArea>
                                <CardMedia
                                    component="img"
                                    height="140"
                                    src={ API + data.attributes.image.data.attributes.url}
                                    alt="-"
                                />
                                <CardContent>
                                    <Grid>
                                    <Grid item xs={12}>
                                        <Link underline="none" href={`../article/${data.attributes.title}`} className={styles.newArticleLinkStyle}>
                                            {data.attributes.title}
                                        </Link>
                                    </Grid>
                                    </Grid>
                                    <Typography className={styles.newArticleDescriptionStyle}>
                                        {data.attributes.subtitle}
                                    </Typography>
                                </CardContent>
                                </CardActionArea>
                            </Card>
                            </motion.div>
                        </Grid>
                        ))}
                    </Grid>
                    </Box>
                </motion.div>
            </Grid>
        </Box>
    );
}

export default NewArticleComponent;