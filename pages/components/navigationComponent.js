import React from 'react';
import Link from '@mui/material/Link';
import styles from '../../styles/Home.module.css'
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';

const pages = [
    'Home',
    'Menu 1',
    'Menu 2',
    'Menu 3',
    'About'
];

function NavigationComponent() {
    const [anchorElNav, setAnchorElNav] = React.useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    return (
        <AppBar position="fixed" style={{ top: 0}}className={styles.appbarStyle}>
            <Container maxWidth="xl">
                <Toolbar disableGutters="disableGutters">
                    <Link href={`/`} underline='none' className={styles.appbarLogoStyle}> Lorem Ipsum</Link>
                    <Box
                        sx={{
                            flexGrow: 1
                        }}/>
                    <Box
                        sx={{
                            display: {
                                xs: 'flex',
                                md: 'none'
                            }
                        }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={handleOpenNavMenu}
                            color="inherit">
                            <MenuIcon/>
                        </IconButton>
                        <Menu
                            id="menu-appbar"
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left'
                            }}
                            keepMounted="keepMounted"
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left'
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloseNavMenu}
                            sx={{
                                display: {
                                    xs: 'block',
                                    md: 'none'
                                }
                            }}>
                            {
                                pages.map((page) => (
                                    <MenuItem key={page} onClick={handleCloseNavMenu}>
                                        <Typography textAlign="center" className={styles.menuAppBarStyle}>{page}</Typography>
                                    </MenuItem>
                                ))
                            }
                        </Menu>
                    </Box>

                    <Box
                        sx={{
                            display: {
                                xs: 'none',
                                md: 'flex'
                            }
                        }}>
                        {
                            pages.map((page) => (
                                <Button
                                    key={page}
                                    onClick={handleCloseNavMenu}
                                    sx={{
                                        my: 2,
                                        color: 'white',
                                        display: 'block'
                                    }}>
                                    {page}
                                </Button>
                            ))
                        }
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
}

export default NavigationComponent;