/* eslint-disable */

import React from 'react';
import Image from 'next/image'
import styles from '../../styles/Home.module.css'
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';

import { motion } from 'framer-motion';
import { useInView } from 'react-intersection-observer';


function FooterComponent() {

    const [ref, inView] = useInView({
        triggerOnce: false,
        threshold: 0.5,
    });

    return (
        <>
            <Box className={styles.footerBoxStyle}>
                <Image
                    src="/images/line-right.png"
                    width={800}
                    height={800}
                    loading="lazy"
                    alt="Image"
                    className={styles.footerLineStyle}/>
                <motion.div
                    ref={ref}
                    className={`popup ${inView ? 'visible' : ''}`}
                    initial={{ opacity: 0, scale: 0.8 }}
                    animate={{ opacity: inView ? 1 : 0, scale: inView ? 1 : 0.8 }}
                    transition={{ duration: 1, ease: 'easeInOut' }}
                >   
                <Paper elevation={0} className={styles.footerPaperStyle}>
                    <Grid container="container">
                        <Grid item="item" xs={4}>
                            <Typography className={styles.footerLogoStyle}>
                                Lorem Ipsum
                            </Typography>
                            <Typography className={styles.footerTextAboutStyle}>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                <br/>
                                Nullam viverra risus vitae libero
                                <br/>
                                Praesent vestibulum, dolor id hendrerit tincidunt.
                            </Typography>
                        </Grid>
                        <Grid item="item" xs={3}>
                            <Typography className={styles.footerTextPrimaryStyle}>
                                Terms & Condition
                            </Typography>
                            <Typography className={styles.footerTextSecondaryStyle}>
                                <Link href="#" underline="none" className={styles.footerLinkStyle}>
                                    Privacy & Security
                                </Link>
                                <Link href="#" underline="none" className={styles.footerLinkStyle}>
                                    Policy
                                </Link>
                                <Link href="#" underline="none" className={styles.footerLinkStyle}>
                                    Require
                                </Link>
                            </Typography>
                        </Grid>
                        <Grid item="item" xs={2}>
                            <Typography className={styles.footerTextPrimaryStyle}>
                                Company
                            </Typography>
                            <Typography className={styles.footerTextSecondaryStyle}>
                                <Link href="#" underline="none" className={styles.footerLinkStyle}>
                                    About Us
                                </Link>
                                <Link href="#" underline="none" className={styles.footerLinkStyle}>
                                    Career
                                </Link>
                                <Link href="#" underline="none" className={styles.footerLinkStyle}>
                                    Event
                                </Link>
                            </Typography>
                        </Grid>
                        <Grid item="item" xs={3}>
                            <Typography className={styles.footerTextPrimaryStyle}>
                                Contact Us
                            </Typography>
                            <Typography className={styles.footerTextContactStyle}>
                                Sed vitae hendrerit odio. Curabitur
                                <br/>
                                Curabitur ac lectus nec felis facilisis
                                <br/>
                                hello@loremipsum.space
                            </Typography>
                        </Grid>
                    </Grid>
                </Paper>
                </motion.div>
            </Box>

            <Box className={styles.endBoxStyle}>
                <Grid container="container">
                    <Grid item="item" xs={12}>
                        <Typography className={styles.endTextSecondaryStyle}>
                            <Link href="#" underline="none" className={styles.endLinkStyle}>
                                2023 &copy; loremipsum.space by daud yusuf
                            </Link>
                        </Typography>
                    </Grid>
                </Grid>
            </Box>
        </>
    );
}

export default FooterComponent;