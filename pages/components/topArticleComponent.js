/* eslint-disable */

import React, { useEffect, useState } from "react";
import { useRouter } from 'next/router';
import Image from 'next/image'
import styles from '../../styles/Home.module.css'
import Box from '@mui/material/Box';
import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import {Carousel} from 'react-responsive-carousel';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import Divider from '@mui/material/Divider';
import ListItemText from '@mui/material/ListItemText';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import {motion} from "framer-motion"
import {useInView} from 'react-intersection-observer';

function TopArticleComponent(props) {
    const API = "http://localhost:1337"
    const router = useRouter();
    const [result, setResult] = useState(props.data);
    const [newArticle, setNewArticle] = useState(props.data);
    const [topArticle, setTopArticle] = useState(null);
    const [ref, inView] = useInView({triggerOnce: false, threshold: 0.5});
    const [searchQuery, setSearchQuery] = useState('');



    //------------------ Trending & New Article Function ------------------//
    useEffect(() => {
        const attributesCount = {};
        result.forEach(item => {
          const title = item.attributes.title; // Ganti dengan atribut yang sesuai, misalnya title
          attributesCount[title] = (attributesCount[title] || 0) + 1;
        });
        const sortedAttributes = Object.keys(attributesCount).sort((a, b) => attributesCount[b] - attributesCount[a]);
        const sortedResult = sortedAttributes.map(title => {
          return result.find(item => item.attributes.title === title);
        });
        setTopArticle(sortedResult.slice(0, 5));
        setNewArticle(result.slice(0, 5));
      }, [result]);

    
    //------------------ Search Article Function ---------------//
    const handleSearch = () => {
        router.push({
            pathname: '/search',
            query: { keyword: (searchQuery) },
        });
    };

    return (
        <Box className={styles.topArticleBoxStyle}>
            <Image
                src="/images/line-right.png"
                width={500}
                height={500}
                loading="lazy"
                alt="Image"
                className={styles.topArticleLineStyle}/>
            <Paper elevation={0} className={styles.topArticlePaperStyle}>
                <motion.div
                    ref={ref}
                    className={`popup ${inView
                        ? 'visible'
                        : ''}`}
                    initial={{
                        opacity: 0,
                        scale: 0.8
                    }}
                    animate={{
                        opacity: inView
                            ? 1
                            : 0,
                        scale: inView
                            ? 1
                            : 0.8
                    }}
                    transition={{
                        duration: 2,
                        ease: 'easeInOut'
                    }}>
                    <Typography className={styles.topArticleTitleStyle}>
                        Top & New Article
                    </Typography>
                </motion.div>
            </Paper>
            <Grid container="container">
                <Grid item="item" xs={6}>
                <motion.div
                    ref={ref}
                    className={`fade ${inView
                        ? 'visible'
                        : ''}`}
                    initial={{
                        opacity: 0
                    }}
                    animate={{
                        opacity: inView
                            ? 1
                            : 0
                    }}
                    transition={{
                        duration: 2,
                        ease: 'easeInOut'
                    }}>
                <Carousel
                    className={styles.articleCarouselStyle}
                    showThumbs={false}
                    autoPlay={true}
                    infiniteLoop={true}
                    interval={4200}>
                    {newArticle != null && newArticle.map ((data) => (
                        <Grid container="container" key={data.id}>
                            <Grid item="item" xs={12} className={styles.topArticleGridStyle}>
                                <div className={styles.overlayCarouselStyle}/>
                                        <Image
                                            src={ API + data.attributes.image.data.attributes.url}
                                            className={styles.articleImgCarouselStyle}
                                            width={800}
                                            height={800}
                                            alt="-"/>
                                    <Link underline="none" href={`../article/${data.attributes.title}`} className={styles.topArticleTextPrimaryStyle}>
                                        {data.attributes.title}
                                    </Link>
                                    <Typography className={styles.topArticleTextSecondaryStyle}>
                                        {data.attributes.subtitle}
                                    </Typography>
                            </Grid>
                        </Grid>
                    ))}
                </Carousel>
                </motion.div>
                </Grid>
                <Grid item="item" xs={6}>
                    <motion.div
                        ref={ref}
                        className={`fade ${inView
                            ? 'visible'
                            : ''}`}
                        initial={{
                            opacity: 0
                        }}
                        animate={{
                            opacity: inView
                                ? 1
                                : 0
                        }}
                        transition={{
                            duration: 2,
                            ease: 'easeInOut'
                        }}>
                        <List className={styles.topArticleStyle} sx={{ bgcolor: 'background.paper' }}>
                        <Box
                        component="form"
                        sx={{
                            '& > :not(style)': { m: 1, width: '32ch' },
                        }}
                        noValidate
                        autoComplete="off"
                        >
                        <TextField 
                            size="small" 
                            id="outlined-basic" 
                            label="Search Article" 
                            variant="outlined"
                            value={searchQuery}
                            onChange={e => setSearchQuery(e.target.value)} />
                        </Box>
                        <Box className={styles.buttonSearchStyle}>
                            <Button 
                                variant="contained"
                                onClick={handleSearch}>
                                Search
                            </Button>
                        </Box>
                        {topArticle != null && topArticle.map ((data) => (
                            <ListItem alignItems="flex-start" key={data.id}>
                                <Link href={`../article/${data.attributes.title}`} underline="none">
                                    <ListItemText
                                    primary={data.attributes.title}
                                    secondary={
                                        <React.Fragment>
                                        <Typography
                                            sx={{ display: 'inline' }}
                                            component="span"
                                            variant="body2"
                                            color="text.primary"
                                        >
                                            {data.attributes.subtitle}
                                        </Typography>
                                        </React.Fragment>
                                    }
                                    />
                                </Link>
                            </ListItem>
                        ))}
                        <Divider variant="inset" component="li" />
                        </List>
                    </motion.div>
                </Grid>
            </Grid>
        </Box>
    );
}

export default TopArticleComponent;