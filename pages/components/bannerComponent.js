/* eslint-disable */

import React from 'react';
import Image from 'next/image'
import styles from '../../styles/Home.module.css'
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';

function BannerComponent() {
    return (
            <Box className={styles.bannerStyle}>
                <div className={styles.overlayStyle}/>
                <Image
                    src="/images/banner.jpg"
                    width={800}
                    height={500}
                    loading="lazy"
                    alt="Image"
                    className={styles.bannerImageStyle}/>
                <Grid container>
                    <Grid item='item' xs={6}>
                        <Typography className={styles.bannerTextPrimaryStyle}>
                        Lorem ipsum dolor sit amet,
                        </Typography>
                        <Typography className={styles.bannerTextSecondaryStyle}>
                        Nullam viverra risus vitae libero feugiat.
                        </Typography>
                        <Typography className={styles.bannerTextSubtitleStyle}>
                            Praesent vestibulum, dolor id hendrerit tincidunt, libero tortor rhoncus purus,
                            <br/>
                            Aliquam erat volutpat, sed dignissim sapien in elit facilisis, id scelerisque nulla varius.
                        </Typography>
                    </Grid>
                </Grid>
            </Box>
    );
}

export default BannerComponent;