import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { CacheProvider } from '@emotion/react';
import theme from '../themes/theme';
import '../styles/globals.css'
import createEmotionCache from '../utils/createEmotionCache';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import Loading from '../utils/loading';

const clientSideEmotionCache = createEmotionCache()

function MyApp(props) {
    const { Component, emotionCache = 
        clientSideEmotionCache, pageProps } = props;

    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const timeout = setTimeout(() => {
          setLoading(false);
        }, 2000);
        return () => clearTimeout(timeout);
      }, []);
  
    return (
        <CacheProvider value={emotionCache}>
            <Head>
                <meta name="viewport" 
                    content="initial-scale=1, width=device-width" />
            </Head>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                { loading ? 
                    <Loading /> : <Component {...pageProps} /> }
            </ThemeProvider>
        </CacheProvider>
    );
}

MyApp.propTypes = {
    Component: PropTypes.elementType.isRequired,
    emotionCache: PropTypes.object,
    pageProps: PropTypes.object.isRequired,
};

export default (MyApp);