import React from 'react';
import Box from '@mui/material/Box';
import CircularProgress from '@mui/material/CircularProgress';

const Loading = () => {
    return (
        <Box className="boxStyle">
            <CircularProgress color="inherit" className='loadingCircullarStyle'/>
            <style global="global" jsx="jsx">
                {
                    ` @font-face {
                        font-family: "Louise";
                        src: url("/fonts/Louise.ttf");
                    }
                    .boxStyle {
                        min-height: 100vh;
                        justify-content: center;
                        background: #0085ff;
                        opacity: 0;
                        animation: fadeIn 500ms ease-in-out forwards;
                    }
                    .loadingCircullarStyle {
                        position: absolute;
                        margin-top: 27%;
                        left: 48.8%;
                        z-index: 1;
                        color: #ffff;
                    }
                    @keyframes floating {
                        0%,
                        100% {
                            transform: translateY(0);
                        }
                        50% {
                            transform: translateY(-20px);
                        }
                    }
                    @keyframes fadeIn {
                      to {
                        opacity: 1;
                      }
                    }
                     `
                }</style>
        </Box>
    );
};

export default Loading;